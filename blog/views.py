from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render, get_object_or_404
#from django.core.urlresolvers import reverse
from taggit.models import Tag
from .models import Post
from .forms import PostForm


# posty blogowe
def post_list(request, tag_slug=None):
    object_list = Post.objects.filter(category__icontains='blogowy').order_by('-published_date')
    tag = None
    all_tags = Tag.objects.all()
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        posts = paginator.page(paginator.num_pages)
    return render(request, 'blog/post_list.html',
                  {'page': page,
                   'posts': posts,
                   'tag': tag,
                   'all_tags': all_tags})


# aktualności
def news_list(request):
    object_list = Post.objects.filter(category__icontains='newsowy').order_by('-published_date')
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        posts = paginator.page(paginator.num_pages)
    return render(request, 'blog/news_list.html', {'posts': posts})


# szczegóły postu
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


# szczegóły newsa
def news_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/news_detail.html', {'post': post})


# nowy post
@login_required()
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('blog_namespace:post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})


# nowy news
@login_required()
def news_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('blog.views.news_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/news_edit.html', {'form': form})


# edycja postu
@login_required()
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            #post.publish()
            post.save()
            return redirect('blog_namespace:post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})


# edycja newsa
@login_required()
def news_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            #post.publish()
            post.save()
            return redirect('blog.views.news_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})