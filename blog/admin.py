from django.contrib import admin
from watson.admin import SearchAdmin
from .models import Post


class PostAdmin(SearchAdmin):
    search_fields = ('title', 'text',)

admin.site.register(Post, PostAdmin)


