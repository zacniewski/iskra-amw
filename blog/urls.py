from django.conf.urls import patterns, url, include
from . import views

urlpatterns = patterns('',
                       url(r'^post/nowy/$', views.post_new, name='post_new'),
                       url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
                       url(r'^post/(?P<pk>[0-9]+)/edycja/$', views.post_edit, name='post_edit'),
                       url(r'^$', views.post_list, name='post_list'),
                       url(r'^tag/(?P<tag_slug>[-\w]+)/$',
                           views.post_list,
                           name='post_list_by_tag'),
)
