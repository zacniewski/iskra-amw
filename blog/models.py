from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
#from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from watson import search as watson
from taggit.managers import TaggableManager


class Post(models.Model):
    CATEGORY_CHOICES = (
        ('blogowy', 'Do blogu'),
        ('newsowy', 'Do aktualności'),
        )
    author = models.ForeignKey(User)
    title = models.CharField(max_length=200)
    text = RichTextUploadingField()
    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        default=timezone.now)
    category = models.CharField(max_length=10,
                                choices=CATEGORY_CHOICES,
                                default='newsowy')

    def publish(self):
        self.published_date = timezone.now
        #self.save()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post_detail',
                       args=[self.pk])

    tags = TaggableManager()

"""
Wiki says that it should be done in Django <1.6,
but in my case searching doesn't work without it
https://github.com/etianen/django-watson/wiki
"""
watson.register(Post)
