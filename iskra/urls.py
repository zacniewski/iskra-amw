"""bookmarks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from ckeditor_uploader.views import upload

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^konto/', include('account.urls')),
    # sending emails
    url(r'^kontakt/wyslij-wiadomosc/$', 'account.views.send_email_to_me'),
    url(r'^kontakt/$', 'account.views.contact'),
    # views for 'sekcje'
    url(r'^sekcje/wszystkie/$', 'account.views.sekcje', name='sekcje-wszystkie'),
    url(r'^sekcje/zeglarstwo/$', 'account.views.zeglarstwo', name='sekcje-zeglarstwo'),
    url(r'^sekcje/lekkoatletyka/$', 'account.views.lekkoatletyka', name='sekcje-lekkoatletyka'),
    url(r'^sekcje/futsal/$', 'account.views.futsal', name='sekcje-futsal'),
    url(r'^sekcje/plywanie/$', 'account.views.plywanie', name='sekcje-plywanie'),
    url(r'^sekcje/badminton/$', 'account.views.badminton', name='sekcje-badminton'),

    url(r'^$', 'account.views.main_page', name='main_page'),
    # calendar from django-calendarium
    url(r'^kalendarz/', include('calendarium.urls')),
    # now logged user (not only staff members) can upload images
    url(r'^ckeditor/upload/',
        login_required(upload),
        name='ckeditor_upload'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^blog/', include('blog.urls',
                           namespace='blog_namespace',
                           app_name='blog')),
    url(r'^newsy/$', 'blog.views.news_list'),
    url(r'^newsy/nowy/$', 'blog.views.news_new', name='news_new'),
    url(r'^newsy/news/(?P<pk>[0-9]+)/$', 'blog.views.news_detail', name='news_detail'),
    url(r'^newsy/news/(?P<pk>[0-9]+)/edycja/$', 'blog.views.news_edit', name='news_edit'),
    url(r'^ciekawostki/', include('sharings.urls', namespace='sharings')),
    url(r"^search/", include("watson.urls", namespace="watson")),


]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)