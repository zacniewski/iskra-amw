"""
WSGI config for iskra project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
from sys import path as p
path = '/home/sait/sait-iskra-amw'
if path not in p:
    p.append(path)
os.environ["DJANGO_SETTINGS_MODULE"] = "iskra.settings"

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

application = get_wsgi_application()
application = DjangoWhiteNoise(application)
