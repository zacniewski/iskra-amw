from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from django.views.decorators.http import require_POST, require_GET
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from common.decorators import ajax_required
from .forms import SharingCreateForm
from .models import Sharing


@login_required()
def sharing_create(request):
    if request.method == 'POST':
        form = SharingCreateForm(data=request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            new_item = form.save(commit=False)

            # assign user
            new_item.user = request.user
            new_item.save()
            messages.success(request, 'Dodano zasób z powodzeniem!')
            return redirect(new_item.get_absolute_url())
    else:
        form = SharingCreateForm(data=request.GET)
    return render(request, 'sharings/image/create.html',
                  {'form': form})


def image_detail(request, id, slug):
    image = get_object_or_404(Sharing, id=id, slug=slug)
    return render(request,
                  'sharings/image/detail.html',
                  {'image':image})


@ajax_required
@login_required
@require_POST
def image_like(request):
    image_id = request.POST.get('id')
    action = request.POST.get('action')
    if image_id and action:
        try:
            image = Sharing.objects.get(id=image_id)
            if action=='like':
                image.users_like.add(request.user)
            else:
                image.users_like.remove(request.user)
            return JsonResponse({'status': 'ok'})
        except:
            pass
    return JsonResponse({'status': 'ko'})


def image_list(request):
    images = Sharing.objects.all()
    paginator = Paginator(images, 8)
    page = request.GET.get('page')
    try:
        images = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        images = paginator.page(1)
    except EmptyPage:
        if request.is_ajax():
            # If the request is AJAX and the page is out of range return an empty page
            return HttpResponse('')
        # If page is out of range deliver last page of results
        images = paginator.page(paginator.num_pages)
    if request.is_ajax():
        return render(request,
                      'sharings/image/list_ajax.html',
                      {'images': images})
    return render(request,
                  'sharings/image/list.html',
                   {'images': images})