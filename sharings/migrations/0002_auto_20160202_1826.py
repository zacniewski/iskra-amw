# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sharings', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sharing',
            old_name='user_like',
            new_name='users_like',
        ),
    ]
