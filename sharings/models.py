from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.text import slugify


class Sharing(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='sharings_created')
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255,
                            blank=True)
    url = models.URLField()
    image = models.ImageField(upload_to='images/%Y/%m/%d')
    description = models.TextField(blank=True)
    created = models.DateField(auto_now_add=True,
                               db_index=True)
    users_like = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                       related_name='sharings_liked',
                                       blank=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
            super(Sharing, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('sharings:detail',
                       args=[self.id, self.slug])