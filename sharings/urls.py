from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^tworzenie/$', views.sharing_create, name='create'),
    url(r'^detale/(?P<id>\d+)/(?P<slug>[-\w]+)/$',
        views.image_detail, name='detail'),
    url(r'^polubienia/$', views.image_like, name='like'),
    url(r'^$', views.image_list, name='list'),
    ]