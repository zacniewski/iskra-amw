from django.contrib import admin
from .models import Sharing


@admin.register(Sharing)
class SharingAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'image', 'created', 'user']
    list_filter = ['created']