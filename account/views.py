from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.mail import BadHeaderError, EmailMessage
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.text import slugify
from .forms import LoginForm, UserRegistrationForm, \
    UserEditForm, ProfileEditForm
from .models import Profile


# dashboard of current user
@login_required
def dashboard(request):
    return render(request, 'account/dashboard.html')


# sending mail under 'Kontakt' tab
def send_email_to_me(request):
    fullname = slugify(request.POST.get('fullname'))
    message = request.POST.get('message')
    from_email = request.POST.get('from_email')
    subject = 'Masz wiadomość ze strony KS Iskra od {}'.format(fullname)
    email = EmailMessage(subject, message, 'kontakt@iskra-amw.org.pl',
                         ['I.Kaminski@amw.gdynia.pl'],
                         bcc=['a.zacniewski@interia.eu'],
                         headers={'Reply-To': from_email})
    try:
        email.content_subtype = "html"  # Main content is now text/html
        email.send(fail_silently=False)
    except BadHeaderError:
        return HttpResponse('Invalid header found.')
    return HttpResponseRedirect('/kontakt/')


# log-in
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'],
                                password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Zalogowano poprawnie')
                else:
                    return HttpResponse('Konto zablokowane')
            else:
                return HttpResponse(u'Błędne logowanie')
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form': form})


# register new users
def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # new user (not saved yet)
            new_user = user_form.save(commit=False)
            new_user.set_password(
                user_form.cleaned_data['password'])
            # now, we can save new user
            new_user.save()
            profile = Profile.objects.create(user=new_user)
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
        return render(request,
                      'account/register.html',
                      {'user_form': user_form})


# edit your profile
@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user,
                                 data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile,
                                       data=request.POST,
                                       files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Profil zaktualizowany pomyślnie')
        else:
            messages.error(request, 'Problem z uaktualnieniem profilu')
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request,
                  'account/edit.html',
                  {'user_form': user_form,
                   'profile_form': profile_form})


# view for the main page
def main_page(request):
    return render(request, 'account/main-page.html')


# view for the 'calendar' page
def calendar(request):
    return render(request, 'account/calendar.html')


# contact view
def contact(request):
    return render(request, 'account/contact.html')


# views for the 'sekcje' page
def sekcje(request):
    return render(request, 'account/sekcje.html')


def zeglarstwo(request):
    return render(request, 'account/zeglarstwo.html')


def lekkoatletyka(request):
    return render(request, 'account/lekkoatletyka.html')


def futsal(request):
    return render(request, 'account/pilka-halowa.html')


def plywanie(request):
    return render(request, 'account/plywanie.html')


def badminton(request):
    return render(request, 'account/badminton.html')