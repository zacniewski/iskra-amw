from django.conf.urls import include, url
from . import views

urlpatterns = [
    #url(r'^zaloguj/$', views.user_login, name='login'),
    url(r'zaloguj/$', 'django.contrib.auth.views.login', name='login'),
    url(r'wyloguj/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'wyloguj-i-zaloguj/$', 'django.contrib.auth.views.logout_then_login', name='logout_then_login'),
    url(r'^$', views.dashboard, name='dashboard'),
    # change password urls
    url(r'^password-change/$',
        'django.contrib.auth.views.password_change',
        name='password_change'),
    url(r'^password-change/done/$',
        'django.contrib.auth.views.password_change_done',
        name='password_change_done'),
    url(r'^zarejestruj/$', views.register, name='register'),
    url(r'^edycja/$', views.edit, name='edit'),
    url('social-auth/',
        include('social.apps.django_app.urls', namespace='social')),
    ]